#!/usr/bin/env octave
# See LICENSE file for copyright and license details.

# Clean
clear
close all

# Data
printf("Data\n");
printf("====");

printf("\nBase")
printf("\n---\n")
Bx = 48e-3
By = 61e-3

printf("\nPlataform")
printf("\n---\n")
Px = 46e-3
Py = 50e-3

printf("\nArm")
printf("\n---\n")
R = 150e-3
M = 150e-3
L = 31e-3

printf("\nPosition")
printf("\n--------\n")
x = 0.00
y = 0.03

# Base points
B1x = Bx;
B1y = By;
B2x = -Bx;
B2y = By;
[Bt Br] = cart2pol(B1x, B1y);
[B3x B3y] = pol2cart(Bt+(2*pi/3), Br);
[B5x B5y] = pol2cart(Bt-(2*pi/3), Br);
[Bt Br] = cart2pol(B2x, B2y);
[B4x B4y] = pol2cart(Bt+(2*pi/3), Br);
[B6x B6y] = pol2cart(Bt-(2*pi/3), Br);

B1 = [B1x B1y 0];
B2 = [B2x B2y 0];
B3 = [B3x B3y 0];
B4 = [B4x B4y 0];
B5 = [B5x B5y 0];
B6 = [B6x B6y 0];
B = [B1; B2; B3; B4; B5; B6];

plot([B1x B2x B3x B4x B5x B6x], [B1y B2y B3y B4y B5y B6y],
	"o","markersize", 10, "linewidth", 2)
axis([-0.1 0.1 -0.1 0.1])
title("Base")
grid
print("base.png", "-S1000,1000");

# Platform points
P1x = Px;
P1y = Py;
P2x = -Px;
P2y = Py;
[Pt Pr] = cart2pol(P1x, P1y)
[P3x P3y] = pol2cart(Pt+(2*pi/3), Pr);
[P5x P5y] = pol2cart(Pt-(2*pi/3), Pr);
[Pt Pr] = cart2pol(P2x, P2y);
[P4x P4y] = pol2cart(Pt+(2*pi/3), Pr);
[P6x P6y] = pol2cart(Pt-(2*pi/3), Pr);

P1 = [P1x P1y R];
P2 = [P2x P2y R];
P3 = [P3x P3y R];
P4 = [P4x P4y R];
P5 = [P5x P5y R];
P6 = [P6x P6y R];
P = [P1; P2; P3; P4; P5; P6];
Po = P

plot([P1x P2x P3x P4x P5x P6x], [P1y P2y P3y P4y P5y P6y],
	"o","markersize", 10, "linewidth", 2)
axis([-0.1 0.1 -0.1 0.1])
title("Plataform")
grid
print("platform.png", "-S1000,1000");

# Plataform position
Cx = x;
Cy = y;
Cz = sqrt(R^2-x^2-y^2);
Ci = [0 Cx];
Cj = [0 Cy];
Ck = [0 Cz];
ni = Cx/R;
nj = Cy/R;
nk = Cz/R;
n = [ni nj nk]

# Graphic Center
plot3(Ci, Cj, Ck, "linewidth", 2)
grid on
hold on
tx = linspace(-0.1, 0.1, 41);
ty = linspace(-0.1, 0.1, 41);
[xx, yy] = meshgrid(tx, ty);
tz = (R^2 - xx*Cx - yy*Cy)/(Cz);
mesh(tx, ty, tz);

# Graphic Sphere
[x, y, z] = sphere (40);
mesh(R*x, R*y, R*z);

hidden('off')
axis([-0.1 0.1 -0.1 0.1 0 0.2])
print("position.png", "-S1000,1000");

close all

# Graphic Base
plot3(B(:,1), B(:, 2), B(:, 3),
	"o","markersize", 10, "linewidth", 2)
hold on

# Center Position of plane
plot3([0 0], [0 0], [0 R], "linewidth", 2)

tx = linspace(-0.1, 0.1, 41);
ty = linspace(-0.1, 0.1, 41);
[xx, yy] = meshgrid(tx, ty);
tz = (R*Cz - xx*Cx - yy*Cy)/(Cz);
mesh(tx, ty, tz);

# Vector unitary in plataform
rx = [1 0 1];
rx(3) = -n(1)*rx(1)/n(3)
rx
ry = cross(n,rx)
rz = n;
# Normalizate
rx = rx./sqrt(dot(rx, rx));
ry = ry./sqrt(dot(ry, ry));
rz = rz./sqrt(dot(rz, rz))

P = P(:,1).*rx + P(:,2).*ry + P(:,3).*rz;
P(:,1) = P(:,1)-Cx;
P(:,2) = P(:,2)-Cy;
P(:,3) = P(:,3)+(R-Cz);

plot3([0 0.01*rx(1)], [0 0.01*rx(2)], [0 0.01*rx(3)], "linewidth", 2)
plot3([0 0.01*ry(1)], [0 0.01*ry(2)], [0 0.01*ry(3)], "linewidth", 2)

plot3(P(:,1), P(:, 2), P(:, 3),
	"o","markersize", 10, "linewidth", 2)

for i = 1:6
	plot3(	[B(i,1) P(i,1)],
		[B(i,2) P(i,2)],
		[B(i,3) P(i,3)],
		"linewidth", 0.2)
endfor

# Arm calculate by law of cosines
D = abs(P-B);
c = sqrt(dot(D',D'));
b = M;
a = L;

# Servo position (theta)
beta = acos((a^2+c.^2-b^2)./(2*a.*c))
[st sp sr] = cart2sph(D(:,1), D(:,2), D(:,3));
Lphi = sp'-beta;
theta = rad2deg(Lphi)

# L position
alpha = acos((a^2+c.^2-b^2)./(2*a.*c));

Lp = zeros(6, 3);
[Lp(1,1) Lp(1,2) Lp(1,3)] = sph2cart(2*pi/3,	Lphi(1), L);
[Lp(2,1) Lp(2,2) Lp(2,3)] = sph2cart(pi/3,	Lphi(2), L);
[Lp(3,1) Lp(3,2) Lp(3,3)] = sph2cart(-2*pi/3,	Lphi(3), L);
[Lp(4,1) Lp(4,2) Lp(4,3)] = sph2cart(pi,	Lphi(4), L);
[Lp(5,1) Lp(5,2) Lp(5,3)] = sph2cart(0,		Lphi(5), L);
[Lp(6,1) Lp(6,2) Lp(6,3)] = sph2cart(-pi/3,	Lphi(6), L);
Lp = Lp+B;

for i = 1:6
	plot3(	[B(i,1) Lp(i,1)],
		[B(i,2) Lp(i,2)],
		[B(i,3) Lp(i,3)],
		"linewidth", 2)
endfor

# M position
Mu = P-Lp;
Mud = sqrt(dot(Mu',Mu'));
for i = 1:6
	Mu(i,:) = Mu(i,:)/Mud(i);
endfor
Mp = Mu*M + Lp;

for i = 1:6
	plot3(	[Lp(i,1) Mp(i,1)],
		[Lp(i,2) Mp(i,2)],
		[Lp(i,3) Mp(i,3)],
		"linewidth", 2)
endfor

Po(:,3)	= 0;
P	= P - [0 0 R];
DPo = sqrt(dot(Po', Po'))
DP = sqrt(dot(P', P'))

theta = fix(theta+90)
printf("(%03d,%03d,%03d,%03d,%03d,%03d)\n",
	theta(1), theta(2), theta(3), theta(4), theta(5), theta(6))

grid on
hidden('off')
axis([-0.1 0.1 -0.1 0.1 -0.05 0.15])
print("final_position.png", "-S1000,1000");
